# Orbit Component :: Shell

### Note:

As a result of changes to PyPi Terms, this package is no longer available from the standard PyPi server. In order to use
this package with "pip" either add "-i https://pypi.madpenguin.uk" to your command line, or for transparency;
```bash
# Create or add to ~/.config/pip/pip.conf
[global]
extra-index-url = https://pypi.madpenguin.uk
```

#### This repository contains the default UI shell. Customise this if you want to use a different default screen layout or another graphical toolkit.