import OrbitShell from '@/../node_modules/orbit-component-shell';

export function menu (app, router, menu) {
    const version = app.metadata.version
    const description = app.metadata.description
    app.use(OrbitShell, {
        title: description,
        version: version,
        router: router,
        menu: menu,
        buttons: [{name: '-', text: '', pri: 50}]
    })
}
