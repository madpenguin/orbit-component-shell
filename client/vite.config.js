import { fileURLToPath, URL } from 'node:url'
const path = require('path');
const { defineConfig } = require('vite');
const vue = require('@vitejs/plugin-vue');
const component = 'orbit-component-shell'
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js'
import pkg from './package.json';
const external = Array.from(new Set([
  ...Object.keys(pkg.dependencies || {}),
  ...['vue', 'vue-router', 'pinia']
]))

export default defineConfig({
  build: {
    sourcemap: true,
    lib: {
      entry: path.resolve(__dirname, 'index.js'),
      name: component,
      fileName: (format) => `${component}.${format}.js`,
    },
    rollupOptions: {
      external: external,
      output: [{format: "es"}],
    }
  },
  plugins: [
	  vue(),
	  cssInjectedByJsPlugin({topExecutionPriority: false})
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }  
});
