import { defineStore } from 'pinia'

export const useComponentsStore = defineStore('componentsStore', {
    state: () => {
        return {
            components: new Map()
        }
    },
    actions: {
        add (name, data) {
            this.components.set(name, data)
        },
        list () {
            this.components.forEach((val, key, map) => {
                console.log("<<", key, "|", val.version, "|", val.latest, ">>")
            })
        },
        update_component (socket, components, key) {
            console.warn("2> Updating>", key)
            const component = this.components.get(key)
            console.log("Component>", component)
            socket.value.emit('get_version', key, component.version, (response) => {
                if (response) {
                    console.log("Response>", key, response)
                    component.latest = response.version
                }
                else console.log('invalid return from version check server for ', key)
            })
        },
        update (socket) {
            console.warn("1> Updating>", socket)
            this.components.forEach((val, key, map) => {
                console.log("Val>", val)
                if (val.nocheck) return
                socket.value.emit('get_version', key, val.version, (response) => {
                    if (response) {
                        console.log("Response>", key, response)
                        val.latest = response.version
                    }
                    else
                        console.log('invalid return from version check server')
                })
            })
        }
    },
    useOrmPlugin: false
})